import { Component } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent {
  employees:any[]=[
    {empId:1,empName:'Deekshitha',salary:5770.00,gender:'Female',doj:'2023-08-18',country:'INDIA',emailId:'deekshu@gmail.com',password:'Deekshu@123'},
    {empId:2,empName:'Akshara',salary:8770.00,gender:'Female',doj:'2021-08-11',country:'INDIA',emailId:'akshara@gmail.com',password:'Akshara@589'},
    {empId:3,empName:'Meghana',salary:59070.00,gender:'Female',doj:'2018-05-25',country:'INDIA',emailId:'meghana@gmail.com',password:'Meghana@517'},
    {empId:4,empName:'Deepika',salary:36360.00,gender:'Female',doj:'2020-07-01',country:'INDIA',emailId:'deepika@gmail.com',password:'Deepika@521'},
    {empId:5,empName:'Manasa',salary:36360.00,gender:'Female',doj:'2020-06-01',country:'INDIA',emailId:'manasa@gmail.com',password:'Manasa@521'},
    {empId:6,empName:'Supriya',salary:36360.00,gender:'Female',doj:'2020-05-01',country:'INDIA',emailId:'supriya@gmail.com',password:'Supriya@876'},
  ]
 constructor(){

 }
 ngOnInit(){
}
submit(){
  console.log(this.employees)
}
}

