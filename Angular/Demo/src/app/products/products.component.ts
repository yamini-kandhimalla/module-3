import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent {
  products = [
    {
      name: 'Bluetooth',
      description: 'Made from good quality Grade 4 natural English Willow.',
      price: 599,
      imageUrl: './assets/Bluetooth.webp'
    },

    {
      name: 'Headphone',
      description: 'Made from good quality Grade 4 natural English Willow.',
      price: 999,
      imageUrl: './assets/Headphone.jpeg'
    },

    {
      name: 'Redmi 12 Smartphone',
      description: 'Made from good quality Grade 4 natural English Willow.',
      price: 10499,
      imageUrl: './assets/Mobile.webp'
    },
    
  ];
  }

